const Koa = require("Koa");
const Router = require("koa-router");
const bodyParser = require("koa-bodyparser");
const fs = require("fs");

class CodingChallengeApi {
  restoreMessageData() {
    let messageDataFileName = "./messageData.json";
    if (fs.existsSync(messageDataFileName)) {
      let msgDataRaw = fs.readFileSync(messageDataFileName);
      let messageData = JSON.parse(msgDataRaw);
      return {
        numberOfCalls: messageData.numberOfCalls,
        lastMessage: messageData.lastMessage,
      };
    }

    return {
      numberOfCalls: 0,
      lastMessage: {},
    };
  }
  init() {
    const app = new Koa();
    const router = new Router();

    app.use(bodyParser());

    /*
    Define an array of hardcoded users
    We're violating REST here by maintaining user state
    Normally, a user authentication/authorisation would be 
    performed within the rest method itself
    */
    const users = [
      {
        username: "admin",
        password: "secret",
      },
      {
        username: "user",
        password: "secret",
      },
    ];
    let currentUser = null;

    /*
    Storing number of calls & last message
    In a real scenario, this object would need to be persisted in a DB
    either in this state (where only the last message is required)
    or in multiple message rows which would then be counted
    we would then inititate this variable to the last known state 
    when the server/application is being restarted
    */
    let messageData = this.restoreMessageData();

    /*
    POST /login endpoint
    Depending on the project, logging could be useful for logins/logouts
    especially for counting the unsuccessful login attempts
    to potentially block the account to prevent attacks
    */
    router.post("/login", (ctx) => {
      const { body } = ctx.request;
      /*
        Check user exists
        Token/Oauth/sessionId approach would be more appropriate here 
        instead of sending plaintext password
      */
      const user = users.find(
        (user) =>
          user.username === body.username && user.password === body.password
      );

      if (!user) {
        ctx.status = 401;
        ctx.body = {
          error: "Login failed",
        };
        return;
      }

      // Set the current user
      currentUser = user;

      ctx.status = 200;
      ctx.body = {
        message: "Login successful",
      };
    });

    /*
    POST /logout endpoint
    Would normally check if any user is logged in to
    perform cleanup actions - Remove sessionId/token etc
    */
    router.post("/logout", (ctx) => {
      currentUser = null;

      ctx.status = 200;
      ctx.body = {
        message: "Logout successful",
      };
    });

    /*
    POST /message endpoint
    It would be wise here to check for the existence of lastMessage
    but also for its length, or potentially malicious content
    */
    router.post("/message", (ctx) => {
      if (currentUser === null) {
        ctx.status = 401;
        ctx.body = {
          message: "Please login first",
        };
        return;
      }

      const { body } = ctx.request;

      messageData.numberOfCalls++;
      messageData.lastMessage = body;
      fs.writeFileSync("./messageData.json", JSON.stringify(messageData));

      ctx.body = {
        message: `Message received: ${body.message}`,
      };
    });

    // GET /stats endpoint
    router.get("/stats", (ctx) => {
      // Check if there's a current user and if the current user is admin
      if (!currentUser || currentUser.username !== "admin") {
        ctx.status = 401;
        ctx.body = {
          error: "Unauthorized",
        };
        return;
      }

      ctx.body = messageData;
    });

    app.use(router.routes());
    // This example creates a service over HTTP, for PROD using HTTPS is recommended
    app.listen(3000);
  }
}

const server = new CodingChallengeApi();
server.init();
